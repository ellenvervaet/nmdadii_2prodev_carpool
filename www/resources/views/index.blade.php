<?php
/**
 * Created by PhpStorm.
 * User: Ellen
 * Date: 30/05/2015
 * Time: 11:26
 */
?>

@extends('master.frame')


@section('content')



<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div ng-view></div>

    </div>
</div>

@endsection