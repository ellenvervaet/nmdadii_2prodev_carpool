<?php
/**
 * Created by PhpStorm.
 * User: Ellen
 * Date: 30/05/2015
 * Time: 11:26
 */
?>

 <!DOCTYPE html>
 <html lang="en" ng-app="userApp">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cargo!</title>

	    <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
	    <link href="css/app.css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="scripts/angular.js"></script>

     <!-- ANGULAR -->
     <!-- service en controller wordt voorlopig uit de public folder geladen -->
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
        <script src="js/controllers/mainCtrl.js"></script>
        <script src="js/services/userService.js"></script>
        <script src="js/routes.js"></script>

         <script src="js/app.js"></script>
 </head>

<!-- We vullen onze 'userApp' uit app.js in en onze controller -->
<body class="container"  ng-controller="mainController"><!--ng-app="userApp"-->

<div class="col-md-8 col-md-offset-2">

    <div class="page-header">
        <h2>Cargo!</h2>
        <div ng-view></div>
    </div>

    <form ng-submit="submitUser()" name="registrationForm"> <!-- ng-submit doet een preventDefault en linkt naar ons functie uit mainController -->

        <div class="form-group">
            <input type="text" class="form-control input-sm" name="given_name" ng-model="userData.given_name" placeholder="Voornaam" required>
            <div ng-show="registrationForm.given_name.$error.required && registrationForm.given_name.$touched">Vergeet je voornaam niet</div>
        </div>

        <!-- COMMENT TEXT -->
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="family_name" ng-model="userData.family_name" placeholder="Familienaam" required>
            <div ng-show="registrationForm.family_name.$error.required && registrationForm.family_name.$touched">Vergeet je achternaam niet</div>
        </div>

        <div class="form-group">
            <input type="email" class="form-control input-sm" name="email" ng-model="userData.email" placeholder="Email" required>
            <div ng-show="registrationForm.email.$error.required && registrationForm.email.$touched">Vergeet je mail niet</div>
            <div ng-show="registrationForm.email.$error.email && registrationForm.email.$touched">Fout mail-adres</div>
        </div>

        <div class="form-group">
            <input type="password" class="form-control input-sm" name="password" ng-model="userData.password" placeholder="Wachtwoord" required>
            <div ng-show="registrationForm.password.$error.required && registrationForm.password.$touched">Vergeet je wachtwoord niet</div>
        </div>

        <div class="form-group">
            <input type="password" class="form-control input-sm" name="confirm_password" ng-model="userData.password_verify" placeholder="Wachtwoord herhalen" data-password-verify="userData.password" required>
            <div ng-show="registrationForm.confirm_password.$error.required && registrationForm.confirm_password.$touched">Herhaal je wachtwoord</div>
            <div ng-show="registrationForm.confirm_password.$error.passwordVerify && registrationForm.confirm_password.$touched">Foute combinatie</div>
        </div>

        <div class="form-group">
            <input type="checkbox" class="form-control input-sm" name="licenced" ng-model="userData.licenced">
        </div>

        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary btn-lg" ng-show="registrationForm.$valid">Submit</button>
        </div>
    </form>


    <!-- loading is een variabele in de mainController -->
    <p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>


    <div ng-repeat="user in users">
        <h3>{{ user.given_name }} {{ user.family_name }}</h3>
        <p>id: {{ user.id }}</p>

        <p><a href="#" ng-click="deleteUser(user.id)" class="text-muted">Delete</a></p>
    </div>

</div>

</body>

 </html>