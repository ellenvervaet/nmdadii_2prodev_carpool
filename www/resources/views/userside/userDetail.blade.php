@extends('master.user')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h1>{{ $user->given_name }} {{ $user->family_name }}</h1>
            <hr/>

            <dl>
            <dt>Email:</dt>
            <dd>{{$user->email}}</dd>
            <dt>Geboortedag:</dt>
            <dd>{{$user->birthday}}</dd>
            <dt>In bezit van rijbewijs</dt>
            <dd><?php echo ($user->licenced?'Ja':'Nee') ?></dd>
            <dt>Beschrijving:</dt>
            <dd>{{$user->description}}</dd>
            <dt>Hobby's</dt>
            <dd>{{$user->hobbys}}</dd>
            <!-- $car komt uit een andere tabel, zie UsersController voor meer -->
            @if (isset($user->brand))
            <dt>Auto: </dt>
            <dd>{{ $user->brand }} {{ $user->type }}</dd>
            </dl>
            @endif

            <a class="btn btn-default" href="/deleteUser{{ $realId }}">Verwijder deze gebruiker</a>

		</div>
	</div>
</div>
@endsection