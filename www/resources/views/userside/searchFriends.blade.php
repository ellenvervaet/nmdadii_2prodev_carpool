@extends('master.user')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">

            <div class="form-group">
                {!! Form::text('vriendZoeken', null, ['class' => 'form-control filterUsers', 'placeholder' => 'Vriend zoeken', 'onkeyup' => 'filterUsers()']) !!}
            </div>

            @if (isset($deletedUser))
                <p>{{ $deletedUser->given_name }} {{ $deletedUser->family_name }} is verwijderd!</p>
                 <a class="btn btn-default" href="/restoreUser{{ $deletedUser->id }}">Herstellen</a>
            @endif

            <ul class="list-group users">
			@foreach($users as $user)
			<li class="list-group-item">
			    <h2>
			        <a href="/user{{ $user->id }}">
			        {{ $user->given_name }} {{$user->family_name}}
			       </a>
			    </h2>

			    @if ($user->licenced == true)
			        <p>Bezit rijbewijs.</p>
			    @else
			        <p>Bezit geen rijbewijs</p>
			    @endif

            </li>
			@endforeach
			</ul>

		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script src="http://www.cargo.arteveldehogeschool.local/js/functions.js"></script>
@endsection