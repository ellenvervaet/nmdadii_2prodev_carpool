@extends('master.loggedOut')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">

                       {!! Form::open(['url' => 'login']) !!}<!--De url is de link waar we naar doorgestuurd willen worden -->

                           <div class="form-group">
                               {!! Form::label('email', 'E-mail:') !!}
                               {!! Form::email('email', null, ['class' => 'form-control']) !!}
                           </div>

                           <div class="form-group">
                               {!! Form::label('wachtwoord', 'Wachtwoord:') !!}
                               {!! Form::password('wachtwoord', ['class' => 'form-control']) !!}
                           </div>

                           @if(Session::has('message'))
                               <p class="error">{{ Session::get('message') }}</p>
                           @endif

                           <div class="form-group">
                               {!! Form::submit('Inloggen', ['class' => 'btn btn-primary form-control']) !!}
                           </div>

                       {!! Form::close() !!}

		</div>
	</div>
</div>
@endsection