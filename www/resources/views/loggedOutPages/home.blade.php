@extends('master.loggedOut')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            <div class="form-group">
                {!! Form::submit('Inloggen', ['class' => 'btn btn-primary form-control', 'onclick' => "window.location='/login'"]) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Registreren', ['class' => 'btn btn-primary btn-yellow form-control', 'onclick' => "window.location='/registreren'"]) !!}
            </div>
        </div>
    </div>
</div>

@endsection