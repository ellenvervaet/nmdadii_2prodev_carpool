@extends('master.loggedOut')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">

            {!! Form::open(['url' => 'registreren']) !!}<!--De url is de link waar we naar doorgestuurd willen worden -->

                <div class="form-group">
                    {!! Form::label('voornaam', 'Voornaam:') !!}
                    {!! Form::text('voornaam', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('achternaam', 'Achternaam:') !!}
                    {!! Form::text('achternaam', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'E-mail:') !!}
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('wachtwoord', 'Wachtwoord:') !!}
                    {!! Form::password('wachtwoord', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('licenced', 'In bezit van rijbewijs:') !!}
                    {!! Form::checkbox('licenced', null, null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Registreren', ['class' => 'btn btn-primary form-control']) !!}
                </div>

            {!! Form::close() !!}

		</div>
	</div>
</div>
@endsection