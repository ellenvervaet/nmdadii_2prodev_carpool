<?php
/**
 * Created by PhpStorm.
 * User: Ellen
 * Date: 1/06/2015
 * Time: 10:00
 */ ?>
 
  <!DOCTYPE html>
  <!-- We verbinden onze 'userApp' uit app.js -->
  <html lang="en" ng-app="userApp">
     <head>
         <meta charset="UTF-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <title>Cargo!</title>

 	     <link href="css/bootstrap.min.css" rel="stylesheet">
         <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
 	     <link href="css/app.css" rel="stylesheet">

         <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
         <script src="scripts/angular.js"></script>
         <script src="scripts/moment.js"></script>
         <!-- angular-route zit vanaf angular 1.1.6 niet meer standaard inbegrepen -->
         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>

         <script src="js/controllers/mainCtrl.js"></script>
         <script src="js/services/userService.js"></script>
         <script src="js/app.js"></script>
  </head>

  <!-- We verbinden onze mainController uit controllers/mainCtrl.js -->
  <body  ng-controller="mainController">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    Cargo!
                </a>
            </div>
        </div>
    </nav>

  @yield('content');

  </body>
</html>