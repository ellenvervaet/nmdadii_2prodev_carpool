<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    const TABLE = 'users';
    const PK = 'id';
    const FK = 'user_id';
    const FK_FRIEND = 'friend_id';
    const FK_CORIDER = 'corider_id';


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
            //Meta Data
			$table->increments(self::PK);
            $table->timestamps();
            $table->softDeletes();

            //Data
            $table->string('given_name');
            $table->string('family_name');
			$table->string('email')
                ->unique();
            $table->string('profile_picture')
                ->nullable();
			$table->string('password', 60);
            $table->timestamp('birthday')
                ->nullable();
            $table->boolean('licenced');
            $table->string('description')
                ->nullable();
            $table->string('hobbys')
                ->nullable();
            $table->timestamp('banned');//als je niet gebanned bent, bevat dit een datum in het verleden
			$table->rememberToken();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE);
	}

}
