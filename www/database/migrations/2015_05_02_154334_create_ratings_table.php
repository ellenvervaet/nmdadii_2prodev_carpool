<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration {

    const TABLE = 'ratings';
    const PK = 'id';
    const FK = 'rating_id';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
            //meta data
			$table->increments(self::PK);

            //data
			$table->integer('total_points');
            $table->integer('total_votes');
            $table->tinyInteger('rating');//this one is calculated by the two proceeding rows
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE);
	}

}
