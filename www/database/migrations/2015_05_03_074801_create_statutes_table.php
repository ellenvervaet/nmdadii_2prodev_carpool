<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateStatutesTable extends Migration {

    const TABLE = 'statutes';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
			//Meta data
            $table->unsignedInteger(CreateUsersTable::FK);
            $table->unsignedInteger(CreateRatingsTable::FK);
            $table->primary([CreateUsersTable::FK, CreateRatingsTable::FK]);

            //foreign key
            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE);
            $table->foreign(CreateRatingsTable::FK)
                ->references(CreateRatingsTable::PK)
                ->on(CreateRatingsTable::TABLE);

            //data
            $table->tinyInteger('profile_completed');
            $table->enum('statute', User::STATUTE_LEVELS);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE);
	}

}
