<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideHasCoriders extends Migration {

    const TABLE = 'ride_has_coriders';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
            //meta data
            $table->unsignedInteger(CreateRidesTable::FK);
            $table->unsignedInteger(CreateUsersTable::FK_CORIDER);
            $table->primary([CreateRidesTable::FK, CreateUsersTable::FK_CORIDER]);

            //foreign keys
            $table->foreign(CreateRidesTable::FK)
                ->references(CreateRidesTable::PK)
                ->on(CreateRidesTable::TABLE);
            $table->foreign(CreateUsersTable::FK_CORIDER)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop(self::TABLE);
	}

}
