<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Friend;

class CreateUserFriendsTable extends Migration {

    const TABLE = 'user_friends';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
            //meta data
            $table->unsignedInteger(CreateUsersTable::FK);
            $table->unsignedInteger(CreateUsersTable::FK_FRIEND);
            $table->primary([CreateUsersTable::FK, CreateUsersTable::FK_FRIEND]);
            $table->timestamps();

            //foreign keys
            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE);
            $table->foreign(CreateUsersTable::FK_FRIEND)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE)
                ->onDelete('cascade');

            //data
            $table->enum('status', Friend::STATUSES);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop(self::TABLE);
	}

}
