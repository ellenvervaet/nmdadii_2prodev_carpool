<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration {

    const TABLE = 'rides';
    const PK = 'id';
    const FK = 'ride_id';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
			//meta data
            $table->increments(self::PK);
			$table->timestamps();
            $table->unsignedInteger(CreateUsersTable::FK);

            //foreign keys
            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE);

            //data
            $table->string('start_place');
            $table->string('arrival_place');
            $table->timestamp('start_time');
            $table->timestamp('arrival_time');
            $table->boolean('music');
            $table->boolean('smoke');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE);
	}

}
