<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCarTable extends Migration {

    const TABLE = 'user_car';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE, function(Blueprint $table)
		{
            //primary key
            $table->unsignedInteger(CreateUsersTable::FK);
            $table->unsignedInteger(CreateCarsTable::FK);
            $table->primary([CreateUsersTable::FK, CreateCarsTable::FK]);

            //foreign keys
            $table->foreign(CreateUsersTable::FK)
                ->references(CreateUsersTable::PK)
                ->on(CreateUsersTable::TABLE);
            $table->foreign(CreateCarsTable::FK)
                ->references(CreateCarsTable::PK)
                ->on(CreateCarsTable::TABLE)
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::TABLE);
	}

}
