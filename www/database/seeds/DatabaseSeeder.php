<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        //Deze regel laat mass-assignment toe
		Model::unguard();

		$this->call('UserTableSeeder');
        $this->command->info('User table seeded');

        $this->call('CarsTableSeeder');
        $this->command->info('Car table seeded');

        $this->call('UserCarsTableSeeder');
        $this->command->info('UserCars table seeded');
	}

}


