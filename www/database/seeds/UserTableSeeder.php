<?php

use Carbon\Carbon;
use App\User;

class UserTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'given_name' => 'Maximiliaan',
            'family_name' => 'Owen',
            'email' => 'Max@cargo.be',
            'password' => Hash::make('max'),
            'licenced' => true,
            'banned' => Carbon::now()
        ));

        $faker = Faker\Factory::create();
        for($i = 0; $i < 15; $i++)
        {
            //We maken random een true of false aan zodat we variatie hebben voor de rijbewijzen
            $licenced = rand(0,1) == 1;

            //Eerst random of we wel of niet een geboortedatum geven, dan random geboortedatum
            $birthdayBool = rand(0,1) ==1;
            $date = null;
            if($birthdayBool)
            {
                $int= rand(562055681,962055681);
                $date = date("Y-m-d H:i:s",$int);
            }

            //Wel of geen description en description vullen
            $descriptionBool = rand(0,1) == 1;
            $description = null;
            if($descriptionBool)
            {
                $description = $faker->paragraph($sentences = 3);
            }

            //Wel of geen hobbys en hobby vullen
            /*$hobbyBool = rand(0,1) == 1;
            $hobby = null;
            if($hobbyBool)
            {
                $i = rand(0, 4);
                for($counter = 0; $counter <= $i; $counter++){
                    $hobby += $faker->word() . ", ";
                }
            }*/

            $user = User::create(array(
                'given_name' => $faker->firstName,
                'family_name' => $faker->lastName,
                'email' => $faker->unique()->email,
                'birthday' => $date,
                'password' => Hash::make($faker->word),
                'licenced' => $licenced,
                'description' => $description,
                'banned' => Carbon::now()
            ));
        }
    }
}