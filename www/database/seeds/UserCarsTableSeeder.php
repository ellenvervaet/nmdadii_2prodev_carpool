<?php

use App\Car;
use App\User;
use App\UserCar;

class UserCarsTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('user_car')->delete();

        //Max
        $max = User::first();
        $clio = Car::where('type', 'clio')->firstOrFail();

        $UserCar = new UserCar();
        $UserCar->user()->associate($max);
        $UserCar->car()->associate($clio);
        $UserCar->save();

        //Faker users
        $faker = Faker\Factory::create();

        $licencedUsers = User::where('licenced', 'true')->get();
        $countCars = Car::all()->count();
        foreach($licencedUsers as $user)
        {
            $UserCar = new UserCar();
            $UserCar->user()->associate($user);
            $UserCar->car()->associate(Car::find($faker->numberBetween(1, $countCars)));
            $UserCar->save();
        }

    }
}