<?php

use App\Car;

class CarsTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('cars')->delete();

        $cars = [
            [
                'brand' => 'Volvo',
                'type' => 'Kinetic',
                'seats' => 5
            ],
            [
                'brand' => 'Renault',
                'type' => 'Clio',
                'seats' => 5
            ],
            [
                'brand' => 'Volkswagen',
                'type' => 'Golf',
                'seats' => 5
            ],
            [
                'brand' => 'Ford',
                'type' => 'Fiesta',
                'seats' => 4
            ],
            [
                'brand' => 'Citroën',
                'type' => 'Picasso',
                'seats' => 5
            ]
        ];

        foreach ($cars as $car){
            Car::create(array(
                'brand' => $car['brand'],
                'type' => $car['type'],
                'seats' => $car['seats']
            ));
        }


    }
}