<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;


    use SoftDeletes;
    protected $dates = ['deleted_at'];

    const STATUTE_LEVEL1 = 'rookie';
    const STATUTE_LEVEL2 = 'ervaren';
    const STATUTE_LEVEL3 = 'master';
    const STATUTE_LEVEL4 = 'guru';
    const STATUTE_LEVELS = [
        self::STATUTE_LEVEL1,
        self::STATUTE_LEVEL2,
        self::STATUTE_LEVEL3,
        self::STATUTE_LEVEL4
    ];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'given_name',
        'family_name',
        'email',
        'password',
        'profile_picture',
        'birthday',
        'licenced',
        'description',
        'hobbys',
        'banned'
    ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['remember_token'];

}
