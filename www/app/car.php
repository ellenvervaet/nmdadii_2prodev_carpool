<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cars';

    //Dit schrijven we erbij omdat we geen timestamps zoals created en updated_at willen bijhouden
    //Als we dit zouden wegdoen zou Eloquent deze wel willen opslaan maar we hebben hier geen column voor
    //dus krijgen we een error "unkwown column updated_at in field_list"
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand',
        'type',
        'seats'
    ];

}
