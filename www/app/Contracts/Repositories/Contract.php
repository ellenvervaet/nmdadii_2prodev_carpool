<?php namespace App\Contracts\Repositories;

interface Contract{

    public function get();

    public function getCollection();

}