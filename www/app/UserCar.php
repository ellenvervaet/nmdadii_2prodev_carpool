<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCar extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_car';

    //Dit schrijven we erbij omdat we geen timestamps zoals created en updated_at willen bijhouden
    //Als we dit zouden wegdoen zou Eloquent deze wel willen opslaan maar we hebben hier geen column voor
    //dus krijgen we een error "unkwown column updated_at in field_list"
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'car_id'
    ];

    //Relaties in de db
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function car()
    {
        return $this->belongsTo('App\Car');
    }

}
