<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model {

	const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUSES = [
        self::STATUS_ACCEPTED,
        self::STATUS_PENDING
    ];

}
