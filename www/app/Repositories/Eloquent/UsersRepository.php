<?php namespace App\Repositories\Eloquent;

use App\Contracts\Repositories\UsersContract;
use App\User;

class UsersRepository extends Repository implements UsersContract {

    protected $filtersValid = [];

    protected $includesValid = [];

    protected $sortsValid = [
        'id',
        'given_name',
        'family_name'
    ];

    public function __construct(array $additionalInput = [])
    {
        $this->model = new User();
        $this->query = $this->model->query();
        parent::__construct($additionalInput);
    }

    public function applyFilters()
    {

    }
}