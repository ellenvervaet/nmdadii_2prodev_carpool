<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class IdOnlyTransformer extends TransformerAbstract
{

	public function transform(Model $model)
	{
		return [
			'id' => $model->id,
		];
	}

}
