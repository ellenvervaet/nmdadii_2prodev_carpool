<?php namespace App\Http\Controllers\API;

use App\User;
use App\Car;
use App\UserCar;
//use Request;
use Hash;
use Carbon\Carbon;
use Auth;
use DB;
use \Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use League\Fractal\Pagination\IlluminatePaginatorAdapter as FractalPaginatorAdapter;
use League\Fractal\Resource as FractalResource;
use App\Repositories\Eloquent\UsersRepository;
use App\Transformers\IdOnlyTransformer;
use App\Transformers\GenericTransformer;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UsersController extends Controller {


	public function index()
    {
        //De data wordt gefilterd/bewerkt voor we het aan de gebruiker doorsturen

        $usersRepository = new UsersRepository();

        $users = $usersRepository->getCollection();
        $paginator = $usersRepository->getPaginator();//zodat we niet te veel gebruikers in 1x kunnen laden

        //Collection vraagt alles op, Item slects 1
        //Transformer berwerkt/filterd data voor gebruiker ze ziet
        $resource = new FractalResource\Collection($users, new GenericTransformer);
        $resource->setPaginator(new FractalPaginatorAdapter($paginator));

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    public function show($id)
    {
        $usersRepository = new UsersRepository();

        $user = $usersRepository->find($id);

        if(!$user)
        {
            return response()
                ->json([
                    'errors' => [
                        ['message' => "Gebruiker met id '${id}' bestaat niet."]
                    ]
                ])
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        $resource = new FractalResource\Item($user, new GenericTransformer);

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    //opslaan/toevoegen -> registreren
    public function store(Request $request)
    {
        $storeUserRequest = new StoreUserRequest();
        $rules = $storeUserRequest->rules();//wat is allemaal verplicht? Zie http/request:StoreUserRequest.php

        $data = $this->_getRequestData($request);
        $userData = $data;

        //Hier valideren we nog eens (dubbele email kan hier bv nog opgevangen worden)
        $validator = Validator::make($userData, $rules);
        if ($validator->fails()){
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $user = new User($userData);
        $user->password = \Hash::make($userData['password']);

        if($user->save()){

            //Collection vraagt alles op, Item slects 1
            //Transformer berwerkt/filterd data voor gebruiker ze ziet
            $resource = new FractalResource\Item($user, new IdOnlyTransformer);

            return response()
                ->json($this->_getResponseData($resource))
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    public function update(Request $request)
    {
        $updateUserRequest = new UpdateUserRequest();
        $rules = $updateUserRequest->rules();

        $data = $this->_getRequestData($request);
        $userData = $data;
        $id = $userData['id'];

        $validator = Validator::make($userData, $rules);
        if($validator->fails())
        {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $usersRepository = new UsersRepository();

        $user = $usersRepository->find($id);

        if(!$user)
        {
            return response()
                ->json([
                    'errors' => [
                        ['message' => "Gebruiker met id '${id}' bestaat niet."]
                    ]
                ])
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        $user->fill($userData);

        $user->save();

        $resource = new FractalResource\Item($user, new GenericTransformer);

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }

    //verwijderen via id
    public function destroy($id)
    {
        $usersRepository = new UsersRepository();

        $user = $usersRepository->find($id);

        if(!$user)
        {
            return response()
                ->json([
                    'errors' => [
                        ['message' => "Gebruiker met id '${id}' bestaat niet."]
                    ]
                ])
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        }
        $user->destroy($id);

        $resource = new FractalResource\Item($user, new GenericTransformer);

        return response()
            ->json($this->_getResponseData($resource))
            ->setStatusCode(Response::HTTP_OK);
    }


/*
    //PAGINAS LADEN======================
    //deze functie is eigenlijk een soort index...
    public function friendsList()
    {
        $users = User::all();
        return view('userside.searchFriends', compact('users'));
    }

    public function show($id)
    {
        //het id wordt uit de url gehaald en meegegeven als parameter van de functie
        //eigenlijk zouden we hier findOrFail() moeten gebruiken maar zit er niet in??
        $user = User::find($id);

        //HUN AUTO MEEGEVEN, tabel gelinkt via een tussentabel (many-to-many)
        //Eerst gaan we in de tussentabel 'UserCar' de car_id ophalen die bij onze gebruiker hoort
        //indien de gebruiker er geen heeft zullen we een lege collectie krijgen, dit vangen we op in de if
        $column = ['car_id'];//->get() ontvangt enkel arrays als parameter, dus deze maken we hier aan
        $car_id = UserCar::where('user_id', $id)->get($column);//eloquent collection

        $realId = $id;//omdat we nog met een bug zitten dat de id's wisselen (door inner join?)
        //foutopvanging
        //als je een colom opvraagt van een rij die niet bestaat (sommige gebruikers hebben geen auto)
        //, krijg je een lege collection terug
        if($car_id->count() == 0)
        {
            return view('userside.userDetail', compact('user', 'realId'));
        }
        else
        {
            //we doen een innerjoin en selecteren enkel de user met $id
            $user = DB::table('user_car')
                ->join('users', 'user_car.user_id', '=', 'users.id')
                ->join('cars', 'user_car.car_id', '=', 'cars.id')
                ->where('users.id', $id)
                ->get();

            $user = $user[0];//eloquent collection maar zit nog een niveau te hoog [[$user]]

            return view('userside.userDetail', compact('user', 'realId'));
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete($id);

        $users = User::all();

        $deletedUser = User::onlyTrashed()->where('id', $id)->get();
        $deletedUser = $deletedUser[0];
        return view('userside.searchFriends', compact('users', 'deletedUser'));
    }

    public function restore($id)
    {
        User::onlyTrashed()->where('id', $id)->restore($id);

        $users = User::all();
        return view('userside.searchFriends', compact('users'));
    }

    public function create()
    {
        return view('loggedOutPages.register');
    }

    public function login()
    {
        return view('loggedOutPages.login');
    }

    public function auth()
    {
        $input = Request::all();

        if(Auth::attempt(array(
            'email' => $input['email'],
            'password' => $input['wachtwoord']//het wachtwoord zal automatisch door de functie gehashed worden
        )))
        {
            return redirect('vriendenzoeken');

        }else
        {
            return redirect('login')->with('message', 'De gegevens waren niet correct');
        }
    }*/

}
