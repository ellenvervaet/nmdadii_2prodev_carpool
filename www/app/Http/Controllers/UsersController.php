<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


use App\User;
use App\Car;
use App\UserCar;
use Request;
use Hash;
use Carbon\Carbon;
use Auth;
use DB;
use League\Fractal\Pagination\IlluminatePaginatorAdapter as FractalPaginatorAdapter;
use League\Fractal\Resource as FractalResource;

class UsersController extends Controller {

    //alle gebruikers worden als json teruggestuurd
	public function index()
    {
        return \Response::json(User::get());
    }

    //opslaan/toevoegen -> registreren
    public function store()
    {
        User::create(array(
            'given_name' => \Input::get('given_name'),
            'family_name' => \Input::get('family_name'),
            'email' => \Input::get('email'),
            'password' => \Hash::make(\Input::get('password')),
            'licenced' => \Input::get('licenced'),
            'banned' => Carbon::now()
        ));

        return \Response::json(array('success' => true));
    }

    //verwijderen via id
    public function destroy($id)
    {
        User::destroy($id);

        return \Response::json(array('success' => true));
    }
/*
    //PAGINAS LADEN======================
    //deze functie is eigenlijk een soort index...
    public function friendsList()
    {
        $users = User::all();
        return view('userside.searchFriends', compact('users'));
    }

    public function show($id)
    {
        //het id wordt uit de url gehaald en meegegeven als parameter van de functie
        //eigenlijk zouden we hier findOrFail() moeten gebruiken maar zit er niet in??
        $user = User::find($id);

        //HUN AUTO MEEGEVEN, tabel gelinkt via een tussentabel (many-to-many)
        //Eerst gaan we in de tussentabel 'UserCar' de car_id ophalen die bij onze gebruiker hoort
        //indien de gebruiker er geen heeft zullen we een lege collectie krijgen, dit vangen we op in de if
        $column = ['car_id'];//->get() ontvangt enkel arrays als parameter, dus deze maken we hier aan
        $car_id = UserCar::where('user_id', $id)->get($column);//eloquent collection

        $realId = $id;//omdat we nog met een bug zitten dat de id's wisselen (door inner join?)
        //foutopvanging
        //als je een colom opvraagt van een rij die niet bestaat (sommige gebruikers hebben geen auto)
        //, krijg je een lege collection terug
        if($car_id->count() == 0)
        {
            return view('userside.userDetail', compact('user', 'realId'));
        }
        else
        {
            //we doen een innerjoin en selecteren enkel de user met $id
            $user = DB::table('user_car')
                ->join('users', 'user_car.user_id', '=', 'users.id')
                ->join('cars', 'user_car.car_id', '=', 'cars.id')
                ->where('users.id', $id)
                ->get();

            $user = $user[0];//eloquent collection maar zit nog een niveau te hoog [[$user]]

            return view('userside.userDetail', compact('user', 'realId'));
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete($id);

        $users = User::all();

        $deletedUser = User::onlyTrashed()->where('id', $id)->get();
        $deletedUser = $deletedUser[0];
        return view('userside.searchFriends', compact('users', 'deletedUser'));
    }

    public function restore($id)
    {
        User::onlyTrashed()->where('id', $id)->restore($id);

        $users = User::all();
        return view('userside.searchFriends', compact('users'));
    }

    public function create()
    {
        return view('loggedOutPages.register');
    }

    public function login()
    {
        return view('loggedOutPages.login');
    }

    public function auth()
    {
        $input = Request::all();

        if(Auth::attempt(array(
            'email' => $input['email'],
            'password' => $input['wachtwoord']//het wachtwoord zal automatisch door de functie gehashed worden
        )))
        {
            return redirect('vriendenzoeken');

        }else
        {
            return redirect('login')->with('message', 'De gegevens waren niet correct');
        }
    }*/

}
