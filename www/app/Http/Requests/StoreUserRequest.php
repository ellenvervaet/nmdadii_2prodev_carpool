<?php namespace App\Http\Requests;

use App\User;

class StoreUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
//		return false;
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'given_name'       => 'required',
            'family_name'      => 'required',
			'email'            => 'required|email|unique:users,email',
			'password'         => 'required',
		];
	}

}
