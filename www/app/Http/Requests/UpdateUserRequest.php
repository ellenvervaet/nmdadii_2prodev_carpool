<?php namespace App\Http\Requests;

use App\User;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
//		return false;
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'given_name'       => '',
            'family_name'      => '',
			'email'            => 'email',
			'birthday'         => 'date',
            'licenced'         => '',
            'description'      => '',
            'hobbys'           => '',
		];
	}

}
