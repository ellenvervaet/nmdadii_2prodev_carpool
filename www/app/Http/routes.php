<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Bekijk deze routes via 'php artisan routes'
//Als dit undefined is: 'php artisan route:list'

// HOMEPAGINA ==============================
// We linken hier naar app/views/index.php
// Deze pagina zal al onze Angular content bevatten
Route::get('/', function(){
    //View::make('index');
    return view('index');
});

//API ROUTES ===================================
Route::group(
    array('prefix' => 'api'),
    function(){

        //API v1, zodat bij een nieuwere versie, de eerste versie nog steeds werkt
        Route::group(
            array('prefix' => 'v1' ),
            function(){

                //Soort van API-HOMEPAGE
                Route::get('/', function(){
                    $uriRoot = Request::root(); //haal de home-uri van de site op

                    //De array die zal getoond worden bij home-uri/api/v1/
                    return[
                        "${uriRoot} API" =>[
                            'version' => 1
                        ]
                    ];

                });

                //DATABASE TABLES (opgehaald door controllers)
                Route::resource('users', 'API\UsersController',
                    array('except' => ['create', 'edit']));
            });
});







/*Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('/vriendenzoeken', 'UsersController@friendsList');

Route::get('/registreren', 'UsersController@create');
Route::post('/registreren', 'UsersController@store');

Route::get('/login', 'UsersController@login');
Route::post('/login', 'UsersController@auth');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/user{id}', 'UsersController@show');
Route::get('/deleteUser{id}', 'UsersController@delete');
Route::get('/restoreUser{id}', 'UsersController@restore');*/