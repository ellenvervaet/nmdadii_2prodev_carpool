/**
 * Created by Ellen on 25/05/2015.
 */
function filterUsers()
{
    var li = document.querySelectorAll('.users li');
    for(index = 0; index < li.length; ++index)
    {
        var h2 = li[index].firstChild;
        h2 = h2.nextSibling;
        h2 = h2.firstChild;
        h2 = h2.nextSibling;
        h2 = h2.text;

        var searchTerm = document.querySelector('.filterUsers');
        searchTerm = searchTerm.value;

        if(h2.toLowerCase().includes(searchTerm.toLowerCase()))
        {
            li[index].className = "list-group-item";
        }
        else
        {
            li[index].className = "hidden";
        }

    }
}