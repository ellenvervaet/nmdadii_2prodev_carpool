/**
 * Created by Ellen on 30/05/2015.
 */

angular.module('userService', [])

    //.factory(service name, provider function die een nieuwe instantie van de functie maakt)
.factory('User', function($http){

        //Hier handelen we http-calls naar de API op.
        //Deze functies (get, save, destroy, ...) zullen dus doorverwijzen naar
        // de api-routes die we in laravel routes.php hebben gemaakt
        //We maken gebruik van angular zijn $http service

        return{
            //haal alle gebruikers op
            get : function(){
                  return $http.get('/api/v1/users');
            },

            show : function(id){
              return $http.get('/api/v1/users/' + id);
            },

            //we slaan een gebruiker op
            save : function(userData){
                return $http({
                    method: 'POST',
                    url: '/api/v1/users',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: JSON.stringify(userData)//request content (store gebruikt request en verwacht json)
                });
            },

            update : function(userData, id){
              return $http({
                 method: 'PUT',
                  url: '/api/v1/users/' + id,
                  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'},
                  data: JSON.stringify(userData)
              });
            },

            //we verwijderen een gebruiker via id
            destroy : function(id){
                return $http.delete('api/v1/users/' + id);
            }
        }

    });