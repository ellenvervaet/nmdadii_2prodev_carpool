/**
 * Created by Ellen on 30/05/2015.
 */

var userApp = angular.module('userApp', ['mainCtrl', 'userService', 'ngRoute']);

userApp.directive("passwordVerify", function() {
    console.log('in app.js');
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function(scope, element, attrs, ctrl) {
            scope.$watch(function() {
                var combined;

                if (scope.passwordVerify || ctrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function(value) {
                if (value) {
                    ctrl.$parsers.unshift(function(viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordVerify", false);
                            return undefined;
                        } else {
                            ctrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});


//ROUTES.JS=============================
//Zou eigenlijk in een aparte file moeten zitten
//maar wegens fout die ik niet vind (waarschijnlijk het binden aan de module)
//staat het toch hier.

userApp.config(function($routeProvider){

    $routeProvider
        .when('/',{
            templateUrl: 'pages/index.view.html'
        })
        .when('/registration',{
            templateUrl: 'pages/registration.view.html'
        })
        .when('/userDetail', {
            templateUrl: 'pages/userDetail.view.html'
        })
        .when('/userUpdate', {
            templateUrl: 'pages/userUpdate.view.html'
        });

})
