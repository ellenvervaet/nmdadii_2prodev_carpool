/**
 * Created by Ellen on 30/05/2015.
 */

angular.module('mainCtrl', [])

    //Als 3de parameter injecten we onze userService
    //We gebruiken deze voor zijn functies (get, save, delete, ...)
.controller('mainController', function($scope, $location, $anchorScroll, $http, User){

        //In dit object zullen we de data van de registreren form bewaren
        $scope.userData  = {};

        //Hiermee tonen we een laad-icoontje
        $scope.loadingUsers = true;
        $scope.loadingUser = true;
        $scope.loadingRegistration = false;
        $scope.loadingUpdate = false;

        //USER OPHALEN
        //Haalt alle gebruikers op via de functie die we in userService.js hebben aangemaakt
        //Slaat dit op in $scope.users
        User.get()
            .success(function(data){
                //.success geeft data terug,
                //die data is de array die je ziet in postman.
                //Deze array is echter opgesplits in data en meta.
                //wij hebben enkel data nodig, anders zit onze array een niveau te diep.
                //We halen data eruit door '.data' aan onze data-variabele te voegen.
                $scope.users = data.data;

                //We voegen een extra 'name' toe die zowel de voor- als achternaam bevat voor het zoekveld
                for (index = 0; index < $scope.users.length; ++index)
                {
                    $scope.users[index]['name'] = $scope.users[index]['given_name'] + ' ' + $scope.users[index]['family_name'];
                }

                $scope.loadingUsers = false;
            });

        $scope.showUser = function(id){
            User.show(id)
                .success(function(data){
                    showUser(data);

                })
                .error(function(data){
                   console.log(data);
                });
        };

        //USER OPSLAAN
        //Haalt de data uit het formulier op
        //Slaat dit op in $scope.userData die dan weer doorgegeven wordt aan
        //userService.js die dan weer doorlinkt naar routes.php die dan weer doorlinkt
        //naar usersController.php die de data in de database opslaat
        $scope.submitUser = function(){
            $scope.loadingRegistration = true;

            //Geef de data door aan de functie save die we in userService hebben aangemaakt
            User.save($scope.userData)
                .success(function(data){

                    $location.path('/');

                    //Als het gelukt is, moeten we de de lijst herladen zodat de nieuwe gebruiker erbij staat
                    User.get()
                        .success(function(getData){
                            $scope.users = getData.data;
                            $scope.loadingUsers = false;
                        });

                })
                .error(function(data){
                   console.log(data);
                });

        };

        $scope.updateUser = function(id){

            $scope.loadingUpdate = true;
            $scope.userData['id'] = id;
            $scope.userData['birthday'] = moment($scope.user['birthday']).toDate();

            User.update($scope.userData, id)
                .success(function(data){

                    $location.path('/userDetail');
                    $scope.loadingUpdate = false;
                    $scope.loadingUser = true;

                    User.show($scope.userData['id'])
                        .success(function(data){
                           showUser(data);
                        })
                        .error(function(data){
                           console.log(data);
                        });
                })
                .error(function(data){
                    console.log(data);
                });
        };

        //USER VERWIJDEREN by id
        $scope.deleteUser = function(id, name){
            $scope.loadingUsers = true;

            console.log(name);
            $scope.userDeleted = name;
            console.log($scope.userDeleted);

            //Als je beneden ergens een gebruiker verwijderd, lijkt er eerst niets te gebeuren
            //daarom scrollen we terug naar boven zodat je de spinning-wheel ziet.
            //Bovenaan komt ook de optie om het verwijderen van de gebruiker ongedaan te maken.
            $location.hash('top');
            $anchorScroll();

            //We gebruiken de functie die we in userService.js gemaakt hebben
            // deze gaat naar routes.php -> UserController.php die gebruiker uit db verwijderd
            User.destroy(id)
                .success(function(data){

                    //Als het gelukt is refreshen we de list
                    User.get()
                        .success(function(getData){
                            $scope.users = getData.data;
                            $scope.loadingUsers = false;
                        });

                })
                .error(function(data){
                    $scope.userDeleted = null;
                    console.log(data);
                });
        };

        //Zit in een functie want we gebruiken dit bij Details van gebruiker en
        //dit is bereikbaar via 2 wegen: show of update
        function showUser(data)
        {
            $scope.loadingUser = false;
            $scope.user = data.data;

            //voor als de gebruiker wil updaten, dan staan de basiswaarden al correct
            //deze worden namelijk ook ingevuld in de inputs (two-way-binding) en als
            //we dit niet zouden doen zouden ze nog gelijk staan aan de laatste registration
            $scope.userData = $scope.user;
            //omdat de ng-checked (updateform) test op een bool, niet string
            $scope.userData['licenced'] = $scope.userData['licenced'] = 0 ? false: true;
            //omzetten zodat mooi is voor gebruiker
            if($scope.user['birthday']){
                $scope.user['birthday'] = moment($scope.user['birthday']).format('YYYY/MM/DD');
            }
        }
    });

