/**
 * Created by Ellen on 31/05/2015.
 */
/*
;(function () { 'use strict';

    angular.module('userApp')
        //in plaats van in config je dingen te schrijven, schrijf je de dingen in een functie die je meegeeft
        .config(Routes)
        .run(Preloads);

    // Routes
    // ------
    Routes.$inject = [
        // Angular
        '$routeProvider'
    ];

    function Routes(
        // Angular
        $routeProvider
        ) {
        $routeProvider
            // Splash Screen
            .when('/', {
                templateUrl: _getView('index')
            })

            // Default
            .otherwise({
                redirectTo: '/'
            });
    }

    // Preload Templates
    // -----------------
    Preloads.$inject = [
        // Angular
        '$http',
        '$templateCache'
    ];

    function Preloads(
        // Angular
        $http,
        $templateCache
        ) {
        var views = [
                'pages/index'
            ];

        views.forEach(function (template) {
            $http.get(_getView(template), { cache: $templateCache });
        });
    }

    /**
     *
     * @param uri
     * @returns {string}
     * @private
     *//*
    function _getView(uri) {
        return '/templates/' + uri + '.view.html';
    }

})();
*/