Deze app is gemaakt in opdracht voor de Arteveldehogeschool door:

- Ellen Vervaet
- Michael Yang
- Robbe Schepens

Om de app te laten werken:

- Voeg de site toe in de .yaml file door 'artestead edit' uit te voeren.
- Plaats de map in je Code map
- In de www-map voer je uit:
    - composer self-update
    - composer install
    - composer update
    - npm install
    - bower install
- Zorg nu ook dat je de .env.example kopieërt naar een gewoone .env

- Staat de site in je hosts-file?
- Moet je nog een serve-command uitvoeren?

- Voor de db