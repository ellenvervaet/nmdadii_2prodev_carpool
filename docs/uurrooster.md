Timesheet per student
====================

**Ellen**

- 18/02/15-> 120 minuten: logo schetsen, moodboard, ideaboard (logo, kleurcombinaties, gamificationgeld).
- 23/02/15->  40 minuten: template productiedossier (hoofdstukken en al kleine invullingen), 3 persona’s methun scenarios, concept basis.
- 25/03/15->  40 minuten: onderzoek naar gamification en dit in een bestandje gegoten
- 31/03/15->  90 minuten: analyse naar concurrentie (blablacar, eurostop en karzoo), eerste style tile gestart, andere kleuren in ideaboard gestoken (de uiteindelijk gekozen kleur)
- 02/04/15->  90 minuten: bruikbare icons uit ‘we love icon fonts’ gehaald en naast elkaar geplaats in ideaboard, verder gewerkt aan eerste style tile en andere 2 style tiles begonnen en tijdschema toegevoegd aan moodboard
- 17/04/15-> 120 minuten: sitemap gemaakt (Michael had al een basisschets, hierop verder gewerkt), bij elke pagina duidelijk vermeld welke info erop te vinden is, welke knoppen en welke functie's
- 18/04/15->  20 minuten: moodboard afgewerkt: nog afbeeldingen bijgeplaatst
- 19/04/15->  30 minuten: nieuw laravel project opgezet (nog niet af, problemen met vagrant)
- 24/04/15->  70 minuten: database leerkracht bestudeerd en eigen database in workbench begonnen
- 27/04/15-> 100 minuten: samen met Michael aan de database in workbench verdergewerkt, alles af behalve types bepaald
- 28/04/15->  20 minuten: laravel project laten werken (gelukt)
- 29/04/15->  60 minuten: screen design login zonder validate, login met validatie en registratiepagina
- 30/04/15-> 120 minuten: screen design adminMenu, adminRegister, adminSchorsen-verwijderen, adminBannen, adminBevestigen
- 02/05/15-> 190 minuten: laravel project proberen laten werken (terug kapot, niet kunnen maken), migrations gemaakt (nog niet helemaal af, werkt wel)
- 03/05/15-> 150 minuten: migrations afgewerkt met 2 modellen (nog een fout met de foreign key references)
- 06/05/15->  35 minuten: migrations afgewerkt (fout foreign keys opgelost + admin en statistiek table toegevoegd), reverse engeneer.
- 22/05/15->  90 minuten: gebruiker geseed, bekeken met tinker, bekeken in browser via api
- 23/05/15->  30 minuten: moodboard en ideaboard aangepast aan opmerkingen leerkracht
- 24/05/15-> 420 minuten: faker geïnstalleerd, userstable en carstable grondig geseed, pagina gemaakt die de details van de gekozen user weergeeft, illluminat/html geïnstalleerd, register form gemaakt en laten werken, inlog form gemaakt en laten werken via Auth::attempt, sass proberen laten werken maar mislukt, basis layout aan pagina's gegeven
- 25/05/15-> 350 minuten: vorige code verbeterd door inner join tussen user en cars table, een gebruiker soft-deleten, deze soft-delete ongedaan maken, poster gemaakt
- 30/05/15-> 420 minuten: gebruiker toevoegen, tonen en verwijderen via angular (userService, mainController aangemaakt, routes.php aangepast). Formvalidatie via angularJS af.
- 31/05/15-> 420 minuten: API laten werken (met repositories, transformers uit smu en fractal), angular aan de API aanpassen zodat alle functies terug werken, gestart met ng-view maar wegens lang zoeken op fout enkel de superbasis
- 01/06/15-> 380 minuten: angular views afgewerkt, content over verschillende views.html verpspreid, update form en code aangemaakt, delete in usersController API-friendly gemaakt, gebruiker zoeken terwijl typen vian angular op homepagina

**Michael**

- 16/04/15-> 150 min: Wireframes (Beginnen aanmaken van wireframes: Aanmelden, beschikbare ritten, bevestigingspagina, …)
- 19/04/15-> 90  min: Wireframes (Contact, Detail rit, Details vriend, Gebruiker toevoegen, …)
- 21/04/15-> 150 min: Wireframes (Gegevens wijzigen, home, vrienden, …)
- 23/04/15-> 20  min: Wireframes (Verder werken aan de wireframes)
- 27/04/15-> 100 min: Database, met ellen in de mysqlworkbench verder gewerkt 
- 01/05/15-> 150 min: Screendesigns (Beginnen aanmaken screendesigns: Vrienden, vriendelijst, vrienden zoeken, …)
- 09/05/15-> 60  min: Screendesigns (Verder werken aan de screendesigns: Over de app, detail rit, …)
- 16/05/15-> 320 min: Screendesigns (Andere screendesigns aanmaken: Statistieken, Mijn profiel, gegevens wijzigen, Home usr, home admin, Contact, Bevestigingspagina, beschikbare ritten, ...)
- 18/05/15-> 30  min: Wireframes (aanpassen adhv commentaar van de docent)
- 18/05/15-> 45  min: Screendesigns (Pagina’s bewerken met de nodige aanpassingen, pagina’s aanmaken die vergeten zijn, overbodige pagina’s verwijderen)
- 18/05/15-> 120 min: Screendesigns (Pagina’s aanpassen omdat ze niet mooi waren: Aanmelden, gegevens wijzigen, …)
- 24/05/15-> 180 min: Screendesigns en Wireframes (Overbodige wireframes verwijderen, kleine aanpassingen aan de wireframes zelf: Bevestigingspagina, Statistieken, Home, … Screendesigns aanpassingen aanbrengen, Kleuren van sommige pagina’s aanpassen, Icoontjes van fontawesome gebruiken op pagina’s: ritten, beschikbare ritten, … Finale aanpassingen aanbrengen, Logo veranderen naar nieuwe logo)
